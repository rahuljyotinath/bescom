from config import *
import difflib, os
import re, datetime
import numbers


def proc_leftbottom(file,ws,left_top_lastrow,current_date):
    #print(left_top_lastrow)

    if isinstance(ws.cell(left_top_lastrow+2,0).value, str) and (str.lower("projection") in str.lower(ws.cell(left_top_lastrow+2,0).value)):
        left_bottom_firstRow = left_top_lastrow + 2
    else:
        print("Projection 1 not found")

    for i in range(7, 15):
        if isinstance(ws.cell(left_bottom_firstRow+i,0).value, str) and str.lower("energy consumed") in str.lower(ws.cell(left_bottom_firstRow +i,0).value):
            left_bottom_energyconsumed_row = left_bottom_firstRow + i
            break

    left_bottom_json = {}

    if str.lower("projection") in str.lower(ws.cell(left_bottom_firstRow,1).value.strip(' ')):
        title1 = ws.cell(left_bottom_firstRow,1).value.strip(' ')
    else:
        title1 = ws.cell(left_bottom_firstRow,0).value.strip(' ')

    left_bottom_json["YearProjection-table1"] = {"Title": title1,
                                                "Projection for the year": (ws.cell(left_bottom_firstRow + 2, 3).value) if isinstance(ws.cell(left_bottom_firstRow+2,3).value,numbers.Real) else 'Null',
                                                "Projection per day": (ws.cell(left_bottom_firstRow + 2, 5).value) if isinstance(ws.cell(left_bottom_firstRow+2,5).value,numbers.Real) else 'Null',
                                                "Energy consumed till date(For the year)": (ws.cell(left_bottom_firstRow + 3, 3).value) if isinstance(ws.cell(left_bottom_firstRow+3,3).value,numbers.Real) else 'Null',
                                                "Energy consumed till date per day": (ws.cell(left_bottom_firstRow + 3, 5).value) if isinstance(ws.cell(left_bottom_firstRow+3,5).value,numbers.Real) else 'Null',
                                                "Balance as on date": (ws.cell(left_bottom_firstRow + 4, 3).value) if isinstance(ws.cell(left_bottom_firstRow+4,3).value,numbers.Real) else 'Null',
                                                "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())
                                                }

    if str.lower("projection") in str.lower(ws.cell(left_bottom_firstRow+5,1).value.strip(' ')):
        title2 = ws.cell(left_bottom_firstRow+5,1).value.strip(' ')
    else:
        title2 = ws.cell(left_bottom_firstRow+5,0).value.strip(' ')


    left_bottom_json["YearProjection-table2"] = {"Title": title2,
                                                 "Projection for month": (ws.cell(left_bottom_firstRow+6,3).value) if isinstance(ws.cell(left_bottom_firstRow+6,3).value,numbers.Real) else 'Null',
                                                 "Projection for month per day": (ws.cell(left_bottom_firstRow+6,5).value) if isinstance(ws.cell(left_bottom_firstRow+6,5).value,numbers.Real) else 'Null',
                                                 "Energy consumed till date": (ws.cell(left_bottom_energyconsumed_row,3).value) if isinstance(ws.cell(left_bottom_energyconsumed_row,3).value,numbers.Real) else 'Null',
                                                 "Energy consumed till date per day": (ws.cell(left_bottom_energyconsumed_row,5).value) if isinstance(ws.cell(left_bottom_energyconsumed_row,5).value,numbers.Real) else 'Null',
                                                 "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                                 }
    left_bottom_json["Kar Cons in MU"] = {"Date1": ws.cell(left_bottom_firstRow+1,6).value,
                                            "Value1": (ws.cell(left_bottom_firstRow+1,8).value) if isinstance(ws.cell(left_bottom_firstRow+1,8).value,numbers.Real) else 'Null',
                                            "Date2": ws.cell(left_bottom_firstRow+3,6).value,
                                            "Value2": (ws.cell(left_bottom_firstRow+3,8).value) if isinstance(ws.cell(left_bottom_firstRow+3,8).value,numbers.Real) else 'Null',
                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                            }
    left_bottom_json["State Demand in MW"] = {"Max":(ws.cell(left_bottom_firstRow+2,9).value) if isinstance(ws.cell(left_bottom_firstRow+2,9).value,numbers.Real) else 'Null',
                                              "Min": (ws.cell(left_bottom_firstRow+2,11).value) if isinstance(ws.cell(left_bottom_firstRow+2,11).value,numbers.Real) else 'Null',
                                              "Time-1": (ws.cell(left_bottom_firstRow+4,9).value) if isinstance(ws.cell(left_bottom_firstRow+4,9).value,numbers.Real) else 'Null',
                                              "Time-2": (ws.cell(left_bottom_firstRow+4,11).value) if isinstance(ws.cell(left_bottom_firstRow+4,11).value,numbers.Real) else 'Null',
                                              "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                              }
    left_bottom_json["Frequency"] = {"Max FQ (Hz)": (ws.cell(left_bottom_firstRow+6,9).value) if isinstance(ws.cell(left_bottom_firstRow+6,9).value,numbers.Real) else 'Null',
                                     "Min FQ (Hz)": (ws.cell(left_bottom_firstRow+6,11).value) if isinstance(ws.cell(left_bottom_firstRow+6,11).value,numbers.Real) else 'Null',
                                     "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                     }

    #print(left_bottom_json)

    db.Projections.insert(left_bottom_json)

    return