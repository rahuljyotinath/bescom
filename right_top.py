from config import *
import difflib, os
import re,datetime


def getcorrect_item(name, seq, which_list):
    select_list = which_list[:]
    original_name = name
    name = name.strip(' ').strip(')').strip(' ')
    if str.lower(name) == str.lower(select_list[seq]):  # Correct generation name in excel file.
        correct_name = select_list[seq]
        select_list[seq] = ''
        return (right_reservoir_correct_names[right_reservoir_names.index(correct_name)])

    if difflib.SequenceMatcher(None, str.lower(name), str.lower(select_list[seq])).ratio() > 0.5:  # Slight change but correct order in excel file.
        correct_name = select_list[seq]
        select_list[seq] = ''
        return (right_reservoir_correct_names[right_reservoir_names.index(correct_name)])

    elif next(name for name in select_list if name):
        temp_name = next(name for name in select_list if name)  # Different order. Maybe/maybe not exactly same.
        temp_ratio = difflib.SequenceMatcher(None, str.lower(name), str.lower(temp_name)).ratio()
        for currentname in select_list:
            if currentname == '':
                continue
            if difflib.SequenceMatcher(None, str.lower(name), str.lower(currentname)).ratio() > temp_ratio:
                temp_name = currentname
                temp_ratio = difflib.SequenceMatcher(None, str.lower(name), str.lower(currentname)).ratio()
        if difflib.SequenceMatcher(None, str.lower(name), str.lower(temp_name)).ratio() > 0.5:
            select_list[select_list.index(temp_name)] = ''
            return (right_reservoir_correct_names[right_reservoir_names.index(temp_name)])
    return None

def proc_right_top(file,ws,current_date):
    column_numbers_per_year = [17,23,27] #13,31
    column_numbers_on_date = [20,25,30]
    right_topjson = {}

    for each_col in range(len(column_numbers_per_year)):
        correctname = getcorrect_item(ws.cell(2,column_numbers_per_year[each_col]).value,each_col,right_reservoir_names)
        if correctname!= None:
            if len(ws.cell(3, column_numbers_per_year[each_col]).value)!=0:
                full_resorvoir_level = ws.cell(3,column_numbers_per_year[each_col]).value
                full_resorvoir_level_value = float(full_resorvoir_level.split(" ")[0])
                full_resorvoir_level_unit = full_resorvoir_level.split(" ")[1]
                if "ft" in str.lower(full_resorvoir_level_unit):
                    full_resorvoir_level_value = full_resorvoir_level_value*(0.3048)
            else:
                full_resorvoir_level_value = 0

            right_topjson["RESERVOIR DETAILS"] = {"name": correctname, \
                             "Per yr/On Date": "per year",\
                              "MU amount": right_reservoir_mu[right_reservoir_correct_names.index(correctname)],\
                              "Full Reservoir Level": full_resorvoir_level_value,\
                              "Full Capacity in MCFT": ws.cell(4,column_numbers_per_year[each_col]).value,\
                              "LEVEL": float("%.2f" % round(ws.cell(7,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(7,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "Live Capty(Mcft)": float("%.2f" % round(ws.cell(8,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(8,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "EQ Energy (MU)": float("%.2f" % round(ws.cell(9,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(9,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "% Storage": float("%.2f" % round(ws.cell(10,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(10,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "Inflow(in Cusecs)": float("%.2f" % round(ws.cell(11,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(11,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "Inflow(in MU)": float("%.2f" % round(ws.cell(12,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(12,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "Discharge(Cusecs)": float("%.2f" % round(ws.cell(13,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(13,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "Inflow during the month (in Mcft)": float("%.2f" % round(ws.cell(14,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(14,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "Inflow during the month (in MU)": float("%.2f" % round(ws.cell(15,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(15,column_numbers_per_year[each_col]).value,float) else 'Null',\
                              "IF-Prog": {"date":ws.cell(16,13).value.strip(' '),\
                                          "value":float("%.2f" % round(ws.cell(16,column_numbers_per_year[each_col]).value, 2)) if isinstance(ws.cell(16,column_numbers_per_year[each_col]).value,float) else 'Null'}, \
                             "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())
                            }
            db.ReservoirDetails.insert(right_topjson.values())

            right_topjson["RESERVOIR DETAILS"] = {"name": correctname, \
                            "Per yr/On Date": "On date",\
                            "MU amount": right_reservoir_mu[right_reservoir_correct_names.index(correctname)],\
                              "Full Reservoir Level": full_resorvoir_level_value,\
                              "Full Capacity in MCFT": ws.cell(4,column_numbers_on_date[each_col]).value,\
                              "LEVEL": float("%.2f" % round(ws.cell(7,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(7,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "Live Capty(Mcft)": float("%.2f" % round(ws.cell(8,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(8,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "EQ Energy (MU)": float("%.2f" % round(ws.cell(9,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(9,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "% Storage": float("%.2f" % round(ws.cell(10,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(10,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "Inflow(in Cusecs)": float("%.2f" % round(ws.cell(11,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(11,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "Inflow(in MU)": float("%.2f" % round(ws.cell(12,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(12,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "Discharge(Cusecs)": float("%.2f" % round(ws.cell(13,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(13,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "Inflow during the month (in Mcft)": float("%.2f" % round(ws.cell(14,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(14,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "Inflow during the month (in MU)": float("%.2f" % round(ws.cell(15,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(15,column_numbers_on_date[each_col]).value,float) else 'Null',\
                              "IF-Prog": {"date":ws.cell(16,13).value.strip(' '),\
                                          "value":float("%.2f" % round(ws.cell(16,column_numbers_on_date[each_col]).value, 2)) if isinstance(ws.cell(16,column_numbers_on_date[each_col]).value,float) else 'Null'},\
                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                }
            db.ReservoirDetails.insert(right_topjson.values())

        else:
            print(ws.cell(2,column_numbers_per_year[each_col]).value + " does not exist in Reservoir table. File Number: " + os.path.basename(file))

    right_topjson["RESERVOIR DETAILS"] = {"Total_at_FRL" : float("%.2f" % round(ws.cell(4,31).value, 2)) if isinstance(ws.cell(4,31).value,float) else 'Null', \
                                          "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                          }
    db.ReservoirDetails.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {  "Reservoir_Location" : "Tattihalla",\
                                            "Inflow(in Mcft)": float("%.2f" % round(ws.cell(18,17).value, 2)) if isinstance(ws.cell(18,17).value,float) else 'Null',\
                                            "Inflow Cum for the month": float("%.2f" % round(ws.cell(18,20).value, 2)) if isinstance(ws.cell(18,20).value,float) else 'Null',\
                                            "Discharge(in Mcft)": float("%.2f" % round(ws.cell(18,25).value, 2)) if isinstance(ws.cell(18,25).value,float) else 'Null',\
                                            "Discharge Cum For the month": float("%.2f" % round(ws.cell(18,28).value, 2)) if isinstance(ws.cell(18,28).value,float) else 'Null', \
                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                            }
    db.ReservoirTable2.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {  "Reservoir_Location" : "Bommanahalli",\
                                            "Inflow(in Mcft)": float("%.2f" % round(ws.cell(19,17).value, 2)) if isinstance(ws.cell(19,17).value,float) else 'Null',\
                                            "Inflow Cum for the month": float("%.2f" % round(ws.cell(19,20).value, 2)) if isinstance(ws.cell(19,20).value,float) else 'Null',\
                                            "Discharge(in Mcft)": float("%.2f" % round(ws.cell(19,25).value, 2)) if isinstance(ws.cell(19,25).value,float) else 'Null',\
                                            "Discharge Cum For the month": float("%.2f" % round(ws.cell(19,28).value, 2)) if isinstance(ws.cell(19,28).value,float) else 'Null', \
                                            "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())
                                            }
    db.ReservoirTable2.insert(right_topjson.values())


    right_topjson["RESERVOIR DETAILS"] = { "title": ws.cell(21,13).value.strip(' '),\
                                           "Energy_Present_Year":float("%.2f" % round(ws.cell(21,23).value, 2)) if isinstance(ws.cell(21,23).value,float) else 'Null',\
                                           "Energy_Percentage_Present_Year":float("%.2f" % round(ws.cell(21,25).value, 2)) if isinstance(ws.cell(21,25).value,float) else 'Null',\
                                           "Energy_Previous_Year":float("%.2f" % round(ws.cell(21,26).value, 2)) if isinstance(ws.cell(21,26).value,float) else 'Null',\
                                           "Energy_Percentage_Previous_Year":float("%.2f" % round(ws.cell(21,28).value, 2)) if isinstance(ws.cell(21,28).value,float) else 'Null',\
                                           "Excess": float("%.2f" % round(ws.cell(21,30).value, 2)) if isinstance(ws.cell(21,30).value,float) else 'Null', \
                                           "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())
                                        }
    db.RSVR_Availability_Total.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {"title":str(ws.cell(22,13).value).strip(' '),\
                                          "days_in_MU":float("%.2f" % round(ws.cell(22,18).value, 2)) if isinstance(ws.cell(22,18).value,float) else 'Null',\
                                          "Energy_Present_Year": float("%.2f" % round(ws.cell(22,23).value, 2)) if isinstance(ws.cell(22,23).value,float) else 'Null',\
                                          "Energy_Previous_Year":float("%.2f" % round(ws.cell(22,26).value, 2)) if isinstance(ws.cell(22,26).value,float) else 'Null',\
                                          "Excess":float("%.2f" % round(ws.cell(22,30).value, 2)) if isinstance(ws.cell(22,30).value,float) else 'Null', \
                                          "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())
                                          }
    db.RSVR_Availability_Perday.insert(right_topjson.values())

    cum_date = ws.cell(23,22).value if len(ws.cell(23,22).value)>0 else 'Null'
    cum_date = str(cum_date)
    right_topjson["RESERVOIR DETAILS"] = {  "Major_Hydro_OnDate": float("%.2f" % round(ws.cell(24,19).value, 2)) if isinstance(ws.cell(24,19).value,float) else 'Null',\
                                            "Major_Hydro_CUM_date" : float("%.2f" % round(ws.cell(24,22).value, 2)) if isinstance(ws.cell(24,22).value,float) else 'Null',\
                                            "Major_Hydro_PrYear" : float("%.2f" % round(ws.cell(24,25).value, 2)) if isinstance(ws.cell(24,25).value,float) else 'Null',\
                                            "Major_Hydro_CUM_date" : float("%.2f" % round(ws.cell(24,27).value, 2)) if isinstance(ws.cell(24,27).value,float) else 'Null',\
                                            "Major_CUM_Difference" : float("%.2f" % round(ws.cell(24,30).value, 2)) if isinstance(ws.cell(24,30).value,float) else 'Null',\
                                            "Minor_Hydro_OnDate" : float("%.2f" % round(ws.cell(25,19).value, 2)) if isinstance(ws.cell(25,19).value,float) else 'Null',\
                                            "Minor_Hydro_CUM_date" : float("%.2f" % round(ws.cell(25,22).value, 2)) if isinstance(ws.cell(25,22).value,float) else 'Null',\
                                            "Minor_Hydro_PrYear" : float("%.2f" % round(ws.cell(25,25).value, 2)) if isinstance(ws.cell(25,25).value,float) else 'Null',\
                                            "Minor_Hydro_CUM_date" : float("%.2f" % round(ws.cell(25,27).value, 2)) if isinstance(ws.cell(25,27).value,float) else 'Null',\
                                            "Minor_CUM_Difference" : float("%.2f" % round(ws.cell(25,30).value, 2)) if isinstance(ws.cell(25,30).value,float) else 'Null', \
                                            "Total_OnDate":float("%.2f" % round(ws.cell(26,19).value, 2)) if isinstance(ws.cell(26,19).value,float) else 'Null',\
                                            "Total_CUM_date": float("%.2f" % round(ws.cell(26,27).value, 2)) if isinstance(ws.cell(26,27).value,float) else 'Null', \
                                            "Total_PrYear": float("%.2f" % round(ws.cell(26, 25).value, 2)) if isinstance(ws.cell(26, 25).value, float) else 'Null', \
                                            "Total_CUM_date": float("%.2f" % round(ws.cell(26, 27).value, 2)) if isinstance(ws.cell(26, 27).value, float) else 'Null', \
                                            "Total_CUM_Difference": float("%.2f" % round(ws.cell(26, 30).value, 2)) if isinstance(ws.cell(26, 30).value, float) else 'Null', \
                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                             }
    db.Total_Hydro_Generation.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {"title": str(ws.cell(28,13).value).strip(' '),\
                                          "RTPS": float("%.2f" % round(ws.cell(28,16).value, 2)) if isinstance(ws.cell(28,16).value,float) else 'Null',\
                                          "BTPS": float("%.2f" % round(ws.cell(28,17).value, 2)) if isinstance(ws.cell(28,17).value,float) else 'Null',\
                                          "Average_1": float("%.2f" % round(ws.cell(28,18).value, 2)) if isinstance(ws.cell(28,18).value,float) else 'Null',\
                                          "SGS": float("%.2f" % round(ws.cell(28,20).value, 2)) if isinstance(ws.cell(28,20).value,float) else 'Null',\
                                          "NPH": float("%.2f" % round(ws.cell(28,23).value, 2)) if isinstance(ws.cell(28,23).value,float) else 'Null',\
                                          "VARAHI": float("%.2f" % round(ws.cell(28,25).value, 2)) if isinstance(ws.cell(28,25).value,float) else 'Null',\
                                          "TOTAL": float("%.2f" % round(ws.cell(28,27).value, 2)) if isinstance(ws.cell(28,27).value,float) else 'Null',\
                                          "Average_2": float("%.2f" % round(ws.cell(28,30).value, 2)) if isinstance(ws.cell(28,30).value,float) else 'Null', \
                                          "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                        }
    db.PS_Generation.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {"title": str(ws.cell(29,13).value).strip(' '),\
                                          "RTPS": float("%.2f" % round(ws.cell(29,16).value, 2)) if isinstance(ws.cell(29,16).value,float) else 'Null',\
                                          "BTPS": float("%.2f" % round(ws.cell(29,17).value, 2)) if isinstance(ws.cell(29,17).value,float) else 'Null',\
                                          "Average_1": float("%.2f" % round(ws.cell(29,18).value, 2)) if isinstance(ws.cell(29,18).value,float) else 'Null',\
                                          "SGS": float("%.2f" % round(ws.cell(29,20).value, 2)) if isinstance(ws.cell(29,20).value,float) else 'Null',\
                                          "NPH": float("%.2f" % round(ws.cell(29,23).value, 2)) if isinstance(ws.cell(29,23).value,float) else 'Null',\
                                          "VARAHI": float("%.2f" % round(ws.cell(29,25).value, 2)) if isinstance(ws.cell(29,25).value,float) else 'Null',\
                                          "TOTAL": float("%.2f" % round(ws.cell(29,27).value, 2)) if isinstance(ws.cell(29,27).value,float) else 'Null',\
                                          "Average_2": float("%.2f" % round(ws.cell(29,30).value, 2)) if isinstance(ws.cell(29,30).value,float) else 'Null', \
                                          "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                        }
    db.PS_Generation.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {  "Present_year": {
                                                            "year": ws.cell(30,19).value, \
                                                            "Cumulative_Year": float("%.2f" % round(ws.cell(32, 19).value, 2)) if isinstance(ws.cell(32, 19).value,float) else 'Null', \
                                                            "Average_Year": float("%.2f" % round(ws.cell(32, 22).value, 2)) if isinstance(ws.cell(32, 22).value,float) else 'Null', \
                                                            "Time_period":ws.cell(32,13).value
                                                            },\
                                            "Previous_year": {
                                                "year": ws.cell(30,25).value, \
                                                "Cumulative_PR_Year": float("%.2f" % round(ws.cell(32, 25).value, 2)) if isinstance(ws.cell(32, 25).value, float) else 'Null', \
                                                "Average_PR_Year": float("%.2f" % round(ws.cell(32, 27).value, 2)) if isinstance(ws.cell(32, 27).value, float) else 'Null', \
                                                "Time_period": ws.cell(32, 13).value
                                                },\
                                            "Difference": float("%.2f" % round(ws.cell(32,30).value, 2)) if isinstance(ws.cell(32,30).value, float) else 'Null', \
                                            "Time_period": ws.cell(32, 13).value,\
                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                            }
    db.Progressive_State_Consumption.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {"Present_year": {
                                                            "year": ws.cell(30, 19).value, \
                                                            "Cumulative_Year": float("%.2f" % round(ws.cell(33, 19).value, 2)) if isinstance(ws.cell(33, 19).value,float) else 'Null', \
                                                            "Average_Year": float("%.2f" % round(ws.cell(33, 22).value, 2)) if isinstance(ws.cell(33, 22).value,float) else 'Null', \
                                                            "Time_period": ws.cell(33, 13).value
                                                        }, \
                                            "Previous_year": {
                                                            "year": ws.cell(30, 25).value, \
                                                            "Cumulative_PR_Year": float("%.2f" % round(ws.cell(33, 25).value, 2)) if isinstance(ws.cell(33, 25).value,float) else 'Null', \
                                                            "Average_PR_Year": float("%.2f" % round(ws.cell(33, 27).value, 2)) if isinstance(ws.cell(33, 27).value,float) else 'Null', \
                                                            "Time_period": ws.cell(33, 13).value
                                                            }, \
                                        "Difference": float("%.2f" % round(ws.cell(33, 30).value, 2)) if isinstance(ws.cell(33, 30).value,float) else 'Null', \
                                        "Time_period": ws.cell(33, 13).value, \
                                        "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                        }

    db.Progressive_State_Consumption.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {
                                            "title": ws.cell(34,13).value,\
                                            "CGS_Ex_BUS_Schedule": {
                                                                "title": ws.cell(35,13).value.rstrip(' : '),\
                                                                "value": float("%.2f" % round(ws.cell(35,17).value, 2)) if isinstance(ws.cell(35,17).value,float) else 'Null',\
                                                                "unit": ws.cell(35,18).value.strip(' ')
                                                            },\
                                            "UI": {
                                                "title": ws.cell(35,20).value,\
                                                "value": float("%.2f" % round(ws.cell(35,25).value, 2)) if isinstance(ws.cell(35,25).value,float) else 'Null',\
                                                "unit": ws.cell(35,26).value
                                                },\
                                            "NET_CGS_Import": {
                                                        "title": ws.cell(36, 20).value, \
                                                        "value": float("%.2f" % round(ws.cell(36, 25).value, 2)) if isinstance(ws.cell(36, 25).value, float) else 'Null', \
                                                        "unit": ws.cell(36, 26).value.strip(' ')
                                                         },\
                                            "CUM_UI_Month":{
                                                        "title": ws.cell(37, 20).value, \
                                                        "value": float("%.2f" % round(ws.cell(37, 25).value, 2)) if isinstance(ws.cell(37, 25).value, float) else 'Null', \
                                                        "unit": ws.cell(37, 26).value.strip(' ')
                                                         },\
                                            "CGS_Allocn": {
                                                        "title": ws.cell(35, 27).value, \
                                                        "value": float("%.2f" % round(ws.cell(35, 30).value, 2)) if isinstance(ws.cell(35, 30).value, float) else 'Null', \
                                                        "unit": ws.cell(35, 31).value.strip(' ')
                                                         },\
                                            "CGS_Sch": {
                                                        "title": ws.cell(36, 27).value, \
                                                        "value": float("%.2f" % round(ws.cell(36, 30).value, 2)) if isinstance(ws.cell(36, 30).value, float) else 'Null', \
                                                        "unit": ws.cell(36, 31).value.strip(' ')
                                                         },\
                                            "CUM_UI_Date": {
                                                            "title": ws.cell(37, 27).value, \
                                                            "value": float("%.2f" % round(ws.cell(37, 30).value, 2)) if isinstance(ws.cell(37, 30).value, float) else 'Null', \
                                                            "unit": ws.cell(37, 31).value.strip(' ')
                                                             }, \
                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                            }
    db.Schedule_CGS.insert(right_topjson.values())


    def get_generator_rows(row1,row2,col,ws):
        dict = {}
        for row_no in range(row1,row2+1):
            if len(str(ws.cell(row_no,col).value))>0:
                try:
                    dict[ws.cell(row_no,col).value.split(':')[0].strip(' ')] = ws.cell(row_no,col).value.split(':')[1].strip(' ')
                except:
                    #print(str(row_no)+str(col))
                    dict[str(row_no)] = ws.cell(row_no,col).value
        return dict

    def get_major_lines_rows(row1,row2,ws):
        dict = {}
        for row_no in range(row1,row2+1):
            if len(str(ws.cell(row_no,22).value))>0:
                dict[str(ws.cell(row_no,22).value).strip(' ').replace('.','')]={
                            "Name of the line" : str(ws.cell(row_no,22).value).strip(' ') if len(str(ws.cell(row_no,22).value))>0 else 'Null',\
                            "From" : ws.cell(row_no,26).value,\
                            "To" : ws.cell(row_no,28).value,\
                            "Remarks" : ws.cell(row_no,30).value
                            }
        return dict

    for row_no in range(40,55):
        if "state generator outages" in str.lower(str(ws.cell(row_no,13).value)):
            cgo_last_row = row_no-1

    for row_no in range(45,60):
        if "unscheduled" in str.lower(str(ws.cell(row_no,13).value)):
            sgo_last_row = row_no-1

    for row_no in range(50,65):
        if "rainfall" in str.lower(str(ws.cell(row_no,13).value)):
            rainfall_first_row = row_no




    right_topjson["RESERVOIR DETAILS"] = {
                                        "Planned_Outages": get_generator_rows(40,cgo_last_row,13,ws),\
                                        "Forced_Outages": get_generator_rows(40,cgo_last_row,17,ws), \
                                        "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                        }
    db.Central_Generator_Outages.insert(right_topjson.values())


    right_topjson["RESERVOIR DETAILS"] = {
                                        "Planned_Outages": get_generator_rows(cgo_last_row+1,sgo_last_row,13,ws),\
                                        "Forced_Outages": get_generator_rows(cgo_last_row+1,sgo_last_row+1,17,ws),\
                                        "Planned-Note1":ws.cell(sgo_last_row+1,13).value,\
                                        "Planned-Note2":ws.cell(sgo_last_row+1,15).value, \
                                        "Forced-Note1":ws.cell(sgo_last_row+1,17).value if len(ws.cell(sgo_last_row+1,17).value ) >0 else 'Null',\
                                        "Forced-Note2":ws.cell(sgo_last_row+1,18).value if len(ws.cell(sgo_last_row+1,18).value ) >0 else 'Null',\
                                        "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                        }
    db.State_Generator_Outages.insert(right_topjson.values())


    right_topjson["RESERVOIR DETAILS"] = {
                                            "title":ws.cell(38,22).value.strip(' ').replace('.',''),
                                            "major_lines": get_major_lines_rows(40,rainfall_first_row,ws), \
                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                            }
    db.Major_Lines.insert(right_topjson.values())
    rainfall_list_init = ws.cell(rainfall_first_row,13).value.split(',')  #58
    if len(rainfall_list_init)>1:
        title = str(rainfall_list_init[0].split(':')[0])
        rainfall_list_init[0] = str(rainfall_list_init[0]).replace(";", ":")
        rainfall_list_init[0] = str(rainfall_list_init[0].split(':')[1]) + ":" + str(rainfall_list_init[0].split(':')[2])
        dict = {}

        for values in range(0, len(rainfall_list_init)):
            if rainfall_list_init[values] != ' ' or '':
                try:
                    dict[str(rainfall_list_init[values].split(':')[0])] = rainfall_list_init[values].split(':')[1]
                except:
                    db.exec_errors.insert_one({
                                            'error_value': rainfall_list_init[values].split(':'),
                                             "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time()),
                                             'datetime_of_error': datetime.datetime.now()})

        dict["date_excel"]= datetime.datetime.combine(current_date, datetime.datetime.min.time())
        right_topjson["RESERVOIR DETAILS"] = {title:dict}
        db.rainfall.insert(right_topjson.values())

        rainfall_daily_cumulative = ws.cell(rainfall_first_row+1,13).value.split(',')
        title = rainfall_daily_cumulative[0].split('}')[0].strip('{')
        dict = {}
        try:
            dict[rainfall_daily_cumulative[0].split('}')[1].strip(' ').split(':')[0]]=rainfall_daily_cumulative[0].split('}')[1].strip(' ').split(':')[1]
        except:
            db.exec_errors.insert_one({'error_value':rainfall_daily_cumulative[0].split('}')[1].strip(' ').split(':'),"date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time()), 'datetime_of_error': datetime.datetime.now()})

        for values in range(1,len(rainfall_daily_cumulative)):
            try:
                dict[rainfall_daily_cumulative[values].strip(' ').strip(' ').split(':')[0]] = rainfall_daily_cumulative[values].strip(' ').strip(' ').split(':')[1]
            except:
                db.exec_errors.insert_one({'error_value': rainfall_daily_cumulative[values],
                                           "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time()),
                                           'datetime_of_error': datetime.datetime.now()})

        dict["date_excel"] = datetime.datetime.combine(current_date, datetime.datetime.min.time())
        right_topjson["RESERVOIR DETAILS"] = {title: dict}
        db.rainfall_daily.insert(right_topjson.values())
    else:
        db.exec_errors.insert_one({'error_value': str(len(rainfall_list_init)) + " is not greater than 1",\
                                   "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time()),\
                                   'datetime_of_error': datetime.datetime.now()})
    temp_list = ws.cell(rainfall_first_row+2, 13).value.split('  ')#.remove('')
    if ' ' in temp_list:
        temp_list = temp_list.remove(' ')
    if temp_list is not None:
        dam_list = [i for i in temp_list if i]
    i=2
    while(True):
        temp_list = ws.cell(rainfall_first_row+i, 13).value.split('  ')
        dam_list = [i for i in temp_list if i]
        if dam_list is None:
            i+=1
            continue
        else:
            break
    #dam_list = dam_list.remove('')
    while ("" in dam_list):
        dam_list.remove("")
    while (' ' in dam_list):
        dam_list.remove(' ')
    value_list =[]
    for dam in range(1,len(dam_list)):
        value = re.sub("[^0-9.\-]","", dam_list[dam])
        value = value[:-1] if str(value).endswith(".") else value
        #print(value)
        #value = re.findall("\d+[\.\d+]?", dam_list[dam])
        if "ft" in str.lower(dam_list[dam]):
            value = float(value) * 0.3048
        value_list.append(value)

    right_topjson["RESERVOIR DETAILS"] = {
                                             "T 'KALALE":value_list[0], \
                                            "VARAHI":value_list[1],\
                                            "BP DAM": value_list[2], \
                                            "S T R P":value_list[3],\
                                            "KADRA":value_list[4], \
                                            "K SALLY":value_list[5], \
                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                        }
    db.dam_pickup_level.insert(right_topjson.values())

    right_topjson["RESERVOIR DETAILS"] = {
                                         "Jindal_to_Outside_state": {
                                                                     "Schedule":float("%.2f" % round(ws.cell(rainfall_first_row+5, 23).value, 2)) if isinstance(ws.cell(rainfall_first_row+5, 23).value, float) else 'Null',\
                                                                     "Actual": float("%.2f" % round(ws.cell(rainfall_first_row+5, 25).value, 2)) if isinstance(ws.cell(rainfall_first_row+5, 25).value, float) else 'Null'
                                                                     }, \
                                         "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                        }
    i=7
    while(str.lower('Total Energy') not in str.lower(ws.cell(rainfall_first_row+i,13).value)):
        right_topjson["RESERVOIR DETAILS"] = {
                                        ws.cell(rainfall_first_row+i,13).value: {
                                                                "On_Date":float("%.2f" % round(ws.cell(rainfall_first_row+i, 23).value, 2)) if isinstance(ws.cell(rainfall_first_row+i, 23).value, float) else 'Null',\
                                                                "Cumulative":float("%.2f" % round(ws.cell(rainfall_first_row+i, 25).value, 2)) if isinstance(ws.cell(rainfall_first_row+i, 25).value, float) else 'Null'
                                                                },\
                                        }
        i+=1

        right_topjson["RESERVOIR DETAILS"] = {
                                        "Total Energy in MU": {
                                                                "On_Date": float("%.2f" % round(ws.cell(rainfall_first_row+i, 23).value, 2)) if isinstance(ws.cell(rainfall_first_row+i, 23).value,float) else 'Null', \
                                                                "Cumulative": float("%.2f" % round(ws.cell(rainfall_first_row+i, 25).value, 2)) if isinstance(ws.cell(rainfall_first_row+i, 25).value,float) else 'Null'
                                                                },\
                                            }
    db.Power_purchased.insert(right_topjson.values())

    return