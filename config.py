import os
from datetime import date, timedelta
from pymongo import MongoClient
from os.path import dirname
from pprint import pprint

excelDirPath = "Excel-Files/"
#excelDirPath = "D:\BESCOM\Excel-Files\\"
# excelProcessed = "D:\BESCOM\script\processedfiles\\"
#excelDirPath = "Excel-Files/"

end_date = date.today()
#end_date=date(2013,1,31)
oneDayinterval = timedelta(days=1)

#client = MongoClient('mongodb://bescom:bescom@localhost:27017')
#client = MongoClient('localhost:27017',username='bescom',password='bescom',authMechanism='SCRAM-SHA-1')
client = MongoClient('localhost', 27017)
db = client.bescom
serverStatusResult=db.command("serverStatus")
#pprint(serverStatusResult)

try:
    list(db.Start_date.find())[0]['start_date']
    start_date_unformatted=list(db.Start_date.find())[0]['start_date']
    dates = start_date_unformatted.split("-")
    start_date=date(int(dates[0]),int(dates[1]),int(dates[2]))
except:
    db.Start_date.insert({'start_date':"2011-01-01"})
    start_date = date(2011,1,1)

try:
    list(db.download_Start_date.find())[0]['d_start_date']
    start_date_unformatted=list(db.download_Start_date.find())[0]['d_start_date']
    dates = start_date_unformatted.split("-")
    download_start_date=date(int(dates[0]),int(dates[1]),int(dates[2]))
except:
    db.download_Start_date.insert({'d_start_date':"2011-01-01"})
    download_start_date = date(2011,1,1)


#db.Current_start_date.insert({start_date:})
generation_namelist = ['R.T.P.S.', 'B.T.P.S.', 'Y.D.G.S.', 'SHARAVATHY', 'N.P.H.', 'VARAHI', 'GERUSOPPA', 'ALMATTI', 'KADRA', \
                'KODASALLY', 'SUPA', 'L.D.P.H.', 'BHADRA', 'GHATAPRABA', 'M.D.P.H.', 'M.G.H.E.', 'SIVASAMUDRA', \
                'MUNIRABAD','SHIMSHAPURA', 'TB Dam Share','SOLAR(KPCL)','Jurala','YTPS']

generation_types_list = ['undefined', 'thermal', 'hydro', 'renewable']

generation_type_dict={
                'R.T.P.S.': generation_types_list[1],
                'B.T.P.S.': generation_types_list[1],
                'Y.D.G.S.': generation_types_list[1],
                'SHARAVATHY': generation_types_list[0],
                'N.P.H.': generation_types_list[0],
                'VARAHI': generation_types_list[0],
                'GERUSOPPA': generation_types_list[0],
                'ALMATTI': generation_types_list[0],
                'KADRA': generation_types_list[0],
                'KODASALLY': generation_types_list[0],
                'SUPA': generation_types_list[0],
                'L.D.P.H.': generation_types_list[0],
                'BHADRA': generation_types_list[0],
                'GHATAPRABA': generation_types_list[0],
                'M.D.P.H.': generation_types_list[0],
                'M.G.H.E.': generation_types_list[0],
                'SIVASAMUDRA': generation_types_list[0],
                'MUNIRABAD': generation_types_list[0],
                'SHIMSHAPURA': generation_types_list[0],
                'TB Dam Share': generation_types_list[0],
                'SOLAR(KPCL)': generation_types_list[0],
                'NET CGS IMPORT': generation_types_list[0],
                'TATA Elec. Co.': generation_types_list[0],
                'RAYAL SEEMA': generation_types_list[0],
                'UPCL': generation_types_list[0],
                'N.C.E Sources': generation_types_list[0],
                'Captive Plants': generation_types_list[0],
                'AP 66 KV': generation_types_list[0],
                'JINDAL':generation_types_list[0],
                'CGS Adjustments+PX Exp':generation_types_list[0],
                'YTPS':generation_types_list[0],
            }

generation_owner_list = ['State Owned', 'Central', 'IPP', 'Other Exchanges']

generation_ownerdict = {
    'R.T.P.S.': generation_owner_list[0],
    'B.T.P.S.': generation_owner_list[0],
    'Y.D.G.S.': generation_owner_list[0],
    'SHARAVATHY': generation_owner_list[0],
    'N.P.H.': generation_owner_list[0],
    'VARAHI': generation_owner_list[0],
    'GERUSOPPA': generation_owner_list[0],
    'ALMATTI': generation_owner_list[0],
    'KADRA': generation_owner_list[0],
    'KODASALLY': generation_owner_list[0],
    'SUPA': generation_owner_list[0],
    'L.D.P.H.': generation_owner_list[0],
    'BHADRA': generation_owner_list[0],
    'GHATAPRABA': generation_owner_list[0],
    'M.D.P.H.': generation_owner_list[0],
    'M.G.H.E.': generation_owner_list[0],
    'SIVASAMUDRA': generation_owner_list[0],
    'MUNIRABAD': generation_owner_list[0],
    'SHIMSHAPURA': generation_owner_list[0],
    'TB Dam Share': generation_owner_list[0],
    'SOLAR(KPCL)': generation_owner_list[0],
    'NET CGS IMPORT': generation_owner_list[1],
    'TATA Elec. Co.': generation_owner_list[2],
    'RAYAL SEEMA': generation_owner_list[2],
    'UPCL': generation_owner_list[2],
    'N.C.E Sources': generation_owner_list[2],
    'Captive Plants': generation_owner_list[2],
    'AP 66 KV': generation_owner_list[3],
    'JINDAL': generation_owner_list[3],
    'CGS Adjustments+PX Exp': generation_owner_list[3],
     'YTPS':generation_owner_list[3],
}


generation_ipps_list = ['NET CGS IMPORT','MSEDCL','UPCL','N.C.E Sources','TATA Elec. Co.','RAYAL SEEMA','Captive Plants','Maharastra system','N.C.E Sources + Solar(10 MW)','Solar','Captive  & Co-Gen','Wind','Mini Hydel','Bio mass','Total NCE','Global','Mini Thermal(Conventional)','Total NCE(PROVISIONAL)']


generation_other_exchanges = ['AP 66 KV','JINDAL','CGS Adjustments+PX Exp']

right_reservoir_names = ['L.MAKKI (4557 MU)','SUPA (3159 MU)','MANI (973 MU)']
right_reservoir_correct_names = ['L.MAKKI', 'SUPA', 'MANI']
right_reservoir_mu = ['4557 MU','3159 MU','973 MU']
right_reservoir_columns=['LEVEL','LIVE CAPTY.(Mcft)','EQ.ENERGY (MU)','% STORAGE','INFLOW ( in Cusecs )',\
                         'INFLOW ( in MU )','DISCHARGE (Cusecs)','Inflow during the month (in Mcft)', \
                        'Inflow during the month (in MU)','I.F.PROG.FROM 1.6.2011 (Mcft)']