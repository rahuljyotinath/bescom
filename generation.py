from config import *
import difflib, os
import re,datetime


def proc_lefttable1_generation(file, ws,current_date):
    print(file)
    merged_cells = []
    for merged_range in ws.merged_cells:
        rlo, rhi, clo, chi = merged_range
        if [rlo, rhi, clo, chi] not in merged_cells:
            merged_cells.append([rlo, rhi, clo, chi])

    temp_generation_name_list = generation_namelist[:]

    def getcorrect_generation_name(name, seq):
        seq -= 1
        if str.lower(name) == str.lower(temp_generation_name_list[seq]):  # Correct generation name in excel file.
            correct_name = temp_generation_name_list[seq]
            temp_generation_name_list[seq] = ''
            return correct_name

        if difflib.SequenceMatcher(None, str.lower(name), str.lower(
                temp_generation_name_list[seq])).ratio() > 0.5:  # Slight change but correct order in excel file.
            correct_name = temp_generation_name_list[seq]
            temp_generation_name_list[seq] = ''
            return correct_name

        elif next(name for name in temp_generation_name_list if name):
            temp_name = next(
                name for name in temp_generation_name_list if name)  # Different order. Maybe/maybe not exactly same.
            temp_ratio = difflib.SequenceMatcher(None, str.lower(name), str.lower(temp_name)).ratio()
            for currentname in temp_generation_name_list:
                if currentname == '':
                    continue
                if difflib.SequenceMatcher(None, str.lower(name), str.lower(currentname)).ratio() > temp_ratio:
                    temp_name = currentname
                    temp_ratio = difflib.SequenceMatcher(None, str.lower(name), str.lower(currentname)).ratio()
            if difflib.SequenceMatcher(None, str.lower(name), str.lower(temp_name)).ratio() > 0.5:
                temp_generation_name_list[temp_generation_name_list.index(temp_name)] = ''
                return temp_name

        return None

    class Generation1Stations:
        def __init__(self, StationName, type=None, installedCapacityNo=None, installedCapacityMW=None, noOfUnits=None,
                     atPeakinMWmax=None,
                     atPeakinMWmin=None, generation4dayMU=None, recMaxMW=None,date_excel=None):
            self.StationName = str(StationName)
            self.type = str(self.getGeneratorType())
            self.owner = str(self.getGeneratorOwner())
            self.installedCapacityNo = (installedCapacityNo)
            self.installedCapacityMW = (installedCapacityMW)
            self.noOfUnits = (noOfUnits)
            self.atPeakinMWmax = (atPeakinMWmax)
            self.atPeakinMWmin = (atPeakinMWmin)
            self.generation4dayMU = (generation4dayMU)
            self.recMaxMW = (recMaxMW)
            self.date_excel = datetime.datetime.combine(current_date, datetime.datetime.min.time())

        def getGeneratorType(self):
            return generation_type_dict.get(self.StationName, "Invalid name")

        def getGeneratorOwner(self):
            return generation_ownerdict.get(self.StationName, "Invalid name")

    if "gen" not in str.lower(ws.cell(17, 0).value):
        print("Error A. Generation file: " + os.path.basename(file))
    if "station" not in str.lower(ws.cell(18, 1).value):
        print("Error stations in file: " + os.path.basename(file))
    if "no" not in str.lower(ws.cell(18, 4).value):
        print("Error column NO in file: " + os.path.basename(file))
    if "mw" not in str.lower(ws.cell(18, 5).value):
        print("Error MW in file: " + os.path.basename(file))
    if "unit" not in str.lower(ws.cell(18, 6).value):
        print("Error No of Units in file: " + os.path.basename(file))
    if "max" not in str.lower(ws.cell(18, 7).value):
        print("Error PEAK MAX in file: " + os.path.basename(file))
    if "min" not in str.lower(ws.cell(18, 8).value):
        print("Error PEAK MIN in file: " + os.path.basename(file))
    if "mu" not in str.lower(ws.cell(18, 9).value):
        print("Error Gen for Day in file: " + os.path.basename(file))
    if "rec" not in str.lower(str(ws.cell(17, 11).value)):
        print("Error REC MAX in file: " + os.path.basename(file))

    #generationtable1json = {}

    if ws.cell(39, 1).value == '':
        end_row = 39
    elif ws.cell(40, 1).value == '':
        end_row = 40
    else:
        end_row = 41

    for row in range(19, end_row):
        correctname = getcorrect_generation_name(ws.cell(row, 1).value, row - 18)
        if correctname != None:
            rowdata = Generation1Stations(StationName=correctname,
                                          installedCapacityNo=("%.2f" % round(ws.cell_value(row, 4), 2)) if isinstance(ws.cell(row, 4).value, float) else 'Null', \
                                          installedCapacityMW=("%.2f" % round(ws.cell_value(row, 5), 2)) if isinstance(ws.cell(row, 5).value, float) else 'Null',\
                                          noOfUnits=("%.2f" % round(ws.cell_value(row, 6), 2)) if isinstance(ws.cell(row, 6).value, float) else 'Null',\
                                          atPeakinMWmax=("%.2f" % round(ws.cell_value(row, 7), 2)) if isinstance(ws.cell(row, 7).value, float) else 'Null', \
                                          atPeakinMWmin=("%.2f" % round(ws.cell_value(row, 8), 2)) if isinstance(ws.cell(row, 8).value, float) else 'Null',\
                                          generation4dayMU=("%.2f" % round(ws.cell_value(row, 9), 2)) if isinstance(ws.cell(row, 9).value, float) else 'Null',\
                                          recMaxMW=("%.2f" % round(ws.cell_value(row, 11), 2)) if isinstance(ws.cell(row, 11).value, float) else 'Null',\
                                          )

            db.Stations.insert(rowdata.__dict__)
        else:
            if len(ws.cell(row,1).value) >0:
                db.execution_errors.insert_one({'file': os.path.basename(file), 'datetime_of_error': datetime.datetime.now(),
                                                'error':(ws.cell(row, 1).value + " does not exist in Generation stations dictionary.")})


    # ~~~~~~~~~~~~~~~~~~ Generation Table A Total row ~~~~~~~~~~~~~~~~~~~

    for i in range(0, 3):
        if ("total" in str.lower(str(ws.cell(end_row + i, 0).value))) or ("total" in str.lower(str(ws.cell(end_row + i, 1).value))):
            generationA_totalrowNo = end_row + i
            break

    totaljson = {}
    totaljson[start_date.strftime("%d%b%Y")] = {"StationInstalledCapacityMW": (float("%.2f" % round(ws.cell_value(generationA_totalrowNo,5),2))) if isinstance(ws.cell(generationA_totalrowNo,5).value,float) else 'Null',
                            "StationPeekMaxInMW": (float("%.2f" % round(ws.cell_value(generationA_totalrowNo,7),2))) if isinstance(ws.cell(generationA_totalrowNo,7).value,float) else 'Null',
                            "StationPeekMinInMW": (float("%.2f" % round(ws.cell_value(generationA_totalrowNo,8),2))) if isinstance(ws.cell(generationA_totalrowNo,8).value,float) else 'Null',
                            "StationDayGenInMU": (float("%.3f" % round(ws.cell_value(generationA_totalrowNo,9),3))) if isinstance(ws.cell(generationA_totalrowNo,9).value,float) else 'Null'}

    # ~~~~~~~~~~~~~~~~~~~ Generation Table B Row ~~~~~~~~~~~~~~~~~~~~

    for i in range(0,4):
        if "net cgs" in str.lower(ws.cell(generationA_totalrowNo+i,1).value):
            generationB_RowNo = generationA_totalrowNo + i
            break
        if difflib.SequenceMatcher(None,"net cgs import" ,str.lower(ws.cell(generationA_totalrowNo+i,1).value)).ratio() > 0.4:
            generationB_RowNo = generationA_totalrowNo + i
            break
    generationB_rowjson = {}

    generationB_rowjson[start_date.strftime("%d%b%Y")] = { "StationName": "NET CGS IMPORT",\
                            "installedCapacityMW": float("%.2f" % round(ws.cell_value(generationB_RowNo,5),2)) if isinstance(ws.cell(generationB_RowNo, 5).value, float) else 'Null',\
                            "atPeakinMWmax": float("%.2f" % round(ws.cell_value(generationB_RowNo,7),2)) if isinstance(ws.cell(generationB_RowNo, 7).value, float) else 'Null',\
                            "atPeakinMWmin": float("%.2f" % round(ws.cell_value(generationB_RowNo,8),2)) if isinstance(ws.cell(generationB_RowNo, 8).value, float) else 'Null',\
                            "generation4dayMU": float("%.3f" % round(ws.cell_value(generationB_RowNo,9),3)) if isinstance(ws.cell(generationB_RowNo, 9).value, float) else 'Null',\
                           "recMaxMW":float("%.2f" % round(ws.cell_value(generationB_RowNo, 11), 2)) if isinstance(ws.cell(generationB_RowNo, 11).value, float) else 'Null', \
                            "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())}

    totaljson[start_date.strftime("%d%b%Y")].update({
                                "CGSInstalledCapacityMW": float("%.2f" % round(ws.cell_value(generationB_RowNo,5),2)) if isinstance(ws.cell(generationB_RowNo, 5).value, float) else 'Null',\
                                "CGSPeekMaxInMW": float("%.2f" % round(ws.cell_value(generationB_RowNo,7),2)) if isinstance(ws.cell(generationB_RowNo, 7).value, float) else 'Null',\
                                "CGSPeekMinInMW": float("%.2f" % round(ws.cell_value(generationB_RowNo,8),2)) if isinstance(ws.cell(generationB_RowNo, 8).value, float) else 'Null',\
                                "CGSDayGenInMU": float("%.3f" % round(ws.cell_value(generationB_RowNo,9),3)) if isinstance(ws.cell(generationB_RowNo, 9).value, float) else 'Null',\
                               "CGSrecMaxMW":float("%.2f" % round(ws.cell_value(generationB_RowNo, 11), 2)) if isinstance(ws.cell(generationB_RowNo, 11).value, float) else 'Null', \
                                })


    db.CGSImport.insert(generationB_rowjson.values())

    # ~~~~~~~~~~~~~~~~~~~~~~~~ Generation Table C (IPP'S) ~~~~~~~~~~~~~~~~~~~~~~~~~~~

    for i in range(1, 5):
        if ws.cell(generationB_RowNo + i, 1).value != '':
            generationC_FirstRowNo = generationB_RowNo + i
            break

    for i in range(0, 15):
        if difflib.SequenceMatcher(None, "total energy from ipp's", str.lower(ws.cell(generationC_FirstRowNo + i, 1).value)).ratio() > 0.5:
            generationC_LastRowNo = generationC_FirstRowNo + i
            break

    temp_generation_ipp_list = generation_ipps_list[:]
    merged_sectionC = [num for num in merged_cells if generationB_RowNo <= num[0] <= generationC_LastRowNo and num[2] <= 11]
    generationC_rowjson = {"IPPImport": {}}
    rows_with_merged_cellsC = list(dict.fromkeys([row_nos for merged_rows in merged_sectionC for row_nos in range(merged_rows[0], merged_rows[1])]))
    temp_merged_sectionC = merged_sectionC[:]


    def merged_cells_process():
        for each_merge in temp_merged_sectionC:
            if ws.cell(each_merge[0],each_merge[2]).value == 'Total NCE':
                temp_merged_sectionC.remove(each_merge)

        while (len(temp_merged_sectionC)>0):
            merge1 = temp_merged_sectionC[0]
            merge_cols = [abc[2:] for abc in merged_sectionC if (merge1[0]==abc[0] and merge1[1]==abc[1])]
            merge_cols_flat_list = list(set([item for sublist in merge_cols for item in range(sublist[0],sublist[1])]))
            merge_cols_flat_list.sort()

            for each_row in range(merge1[0],merge1[1]):

                generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)] = {}
                correctname = getcorrect_generation_ipps(ws.cell(each_row, 1).value, each_row - generationC_FirstRowNo,temp_generation_ipp_list)
                if correctname != None:
                    generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)] = {"StationName": correctname}

                    if 5 not in merge_cols_flat_list:
                        generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)]["installedCapacityMW"] = float("%.2f" % round(ws.cell_value(each_row, 5), 2)) if isinstance(ws.cell(each_row, 5).value,float) else 'Null'
                    if 7 not in merge_cols_flat_list:
                        generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)]["atPeakinMWmax"] = float("%.2f" % round(ws.cell_value(each_row, 7), 2)) if isinstance(ws.cell(each_row, 7).value,float) else 'Null'
                    if 8 not in merge_cols_flat_list:
                        generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)]["atPeakinMWmin"] = float("%.2f" % round(ws.cell_value(each_row, 8), 2)) if isinstance(ws.cell(each_row, 8).value,float) else 'Null'
                    if 9 not in merge_cols_flat_list:
                        generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)]["generation4dayMU"] = float("%.2f" % round(ws.cell_value(each_row, 9), 2)) if isinstance(ws.cell(each_row, 9).value, float) else 'Null'
                    if 11 not in merge_cols_flat_list:
                        generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)]["recMaxMW"] = float("%.2f" % round(ws.cell_value(each_row, 11), 2)) if isinstance(ws.cell(each_row, 11).value, float) else 'Null'
                    generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)]["date_excel"] = datetime.datetime.combine(current_date,datetime.datetime.min.time())
                else:
                    print(ws.cell(each_row,1).value + " does not exist in Generation IPP'S dictionary. File Number: " + os.path.basename(file))

            merged_row_string = ""
            for each_item in range(merge1[0]-generationC_FirstRowNo, merge1[1]-generationC_FirstRowNo):
                merged_row_string += str(each_item) + ","
            merged_row_string = merged_row_string[:-1]
            generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +merged_row_string] = {}
            if 5 in merge_cols_flat_list:
                generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +merged_row_string]["installedCapacityMW"] =  float("%.2f" % round(ws.cell_value(merge1[0], 5), 2)) if isinstance(ws.cell_value(merge1[0], 5), float) else 'Null'
            if 7 in merge_cols_flat_list:
                generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +merged_row_string]["atPeakinMWmax"] =  float("%.2f" % round(ws.cell_value(merge1[0], 7), 2)) if isinstance(ws.cell_value(merge1[0], 7), float) else 'Null'
            if 8 in merge_cols_flat_list:
                generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +merged_row_string]["atPeakinMWmin"] =  float("%.2f" % round(ws.cell_value(merge1[0], 8), 2)) if isinstance(ws.cell_value(merge1[0], 8), float) else 'Null'
            if 9 in merge_cols_flat_list:
                generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +merged_row_string]["generation4dayMU"] =  float("%.2f" % round(ws.cell_value(merge1[0], 9), 2)) if isinstance(ws.cell_value(merge1[0], 9), float) else 'Null'
            if 11 in merge_cols_flat_list:
                generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +merged_row_string]["recMaxMW"] =  float("%.2f" % round(ws.cell(merge1[0], 11).value, 2)) if isinstance(ws.cell(merge1[0], 11).value, float) else 'Null'
            generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +merged_row_string]["date_excel"] = datetime.datetime.combine(current_date, datetime.datetime.min.time())
            current_merged_cols = [x for x in temp_merged_sectionC if (x[0]==merge1[0] and x[1]==merge1[1])]

            for each_item in current_merged_cols:
                temp_merged_sectionC.remove(each_item)



    def getcorrect_generation_ipps(name, seq,select_list):
        seq -= 1
        name = re.sub(r"\d+", "",name)
        name = name.strip(' ').strip(')').strip(' ')
        if str.lower(name) == str.lower(select_list[seq]):  # Correct generation name in excel file.
            correct_name = select_list[seq]
            select_list[seq] = ''
            return correct_name

        if difflib.SequenceMatcher(None, str.lower(name), str.lower(select_list[seq])).ratio() > 0.5:  # Slight change but correct order in excel file.
            correct_name = select_list[seq]
            select_list[seq] = ''
            return correct_name

        elif next(name for name in select_list if name):
            temp_name = next(name for name in select_list if name)  # Different order. Maybe/maybe not exactly same.
            temp_ratio = difflib.SequenceMatcher(None, str.lower(name), str.lower(temp_name)).ratio()
            for currentname in select_list:
                if currentname == '':
                    continue
                if difflib.SequenceMatcher(None, str.lower(name), str.lower(currentname)).ratio() > temp_ratio:
                    temp_name = currentname
                    temp_ratio = difflib.SequenceMatcher(None, str.lower(name), str.lower(currentname)).ratio()
            if difflib.SequenceMatcher(None, str.lower(name), str.lower(temp_name)).ratio() > 0.5:
                select_list[select_list.index(temp_name)] = ''
                return temp_name

        return None


    for each_row in range(generationC_FirstRowNo,generationC_LastRowNo):
        if each_row in rows_with_merged_cellsC:
            continue
        correctname = getcorrect_generation_ipps(ws.cell(each_row, 1).value, each_row - generationC_FirstRowNo,temp_generation_ipp_list)
        if correctname != None:
            generationC_rowjson["IPPImport"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationC_FirstRowNo)] = {"StationName":correctname, \
                                                                                                "installedCapacityMW": float("%.2f" % round(ws.cell_value(each_row,5),2)) if isinstance(ws.cell(each_row,5).value,float) else 'Null',\
                                                                                                "atPeakinMWmax": float("%.2f" % round(ws.cell_value(each_row,7),2)) if isinstance(ws.cell(each_row,7).value,float) else 'Null',\
                                                                                                "atPeakinMWmin": float("%.2f" % round(ws.cell_value(each_row,8),2)) if isinstance(ws.cell(each_row,8).value,float) else 'Null',\
                                                                                                "generation4dayMU":float("%.3f" % round(ws.cell_value(each_row,9),3)) if isinstance(ws.cell(each_row,9).value,float) else 'Null',\
                                                                                               "recMaxMW":float("%.3f" % round(ws.cell_value(each_row,11),3)) if isinstance(ws.cell(each_row,11).value,float) else 'Null',\
                                                                                                "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())}
        else:
            print(ws.cell(each_row,1).value + " does not exist in Generation IPP'S dictionary. File Number: " + os.path.basename(file))


    merged_cells_process()

    totaljson[start_date.strftime("%d%b%Y")].update({"IPPImportInstalledCapacityMW": float("%.2f" % round(ws.cell_value(generationC_LastRowNo, 5), 2)) if isinstance(ws.cell(generationC_LastRowNo, 5).value,float) else 'Null', \
                                                                            "IPPImportPeekMaxInMW": float("%.2f" % round(ws.cell_value(generationC_LastRowNo, 7), 2)) if isinstance(ws.cell(generationC_LastRowNo, 7).value,float) else 'Null', \
                                                                            "IPPImportPeekMinInMW": float("%.2f" % round(ws.cell_value(generationC_LastRowNo, 8), 2)) if isinstance(ws.cell(generationC_LastRowNo, 8).value,float) else 'Null', \
                                                                            "IPPImportDayGenInMU": float("%.3f" % round(ws.cell_value(generationC_LastRowNo, 9), 3)) if isinstance(ws.cell(generationC_LastRowNo, 9).value,float) else 'Null', \
                                                                            "date_excel": datetime.datetime.combine(current_date, datetime.datetime.min.time())
                                                                            #"recMaxMW": ws.cell(generationC_LastRowNo, 11).value
                                                                           })
    db.IPPImport.insert(generationC_rowjson["IPPImport"].values())

    # ~~~~~~~~~~~~~~~~~~~~~~~~ Generation Table D (Other Exchanges) ~~~~~~~~~~~~~~~~~~~~~~~~~~~


    for i in range(1, 5):
        if ws.cell(generationC_LastRowNo + i, 1).value != '':
            generationD_FirstRowNo = generationC_LastRowNo + i
            break

    for i in range(1, 10):
        if (isinstance(ws.cell(generationD_FirstRowNo+i,0).value, str) and str.lower("GRAND TOTAL") in str.lower(ws.cell(generationD_FirstRowNo +i,0).value)):
            generationD_LastRowNo = generationD_FirstRowNo + i
            break

    temp_generation_otherExchanges_list = generation_other_exchanges[:]
    generationD_rowjson = {"OTHER EXCHANGES": {}}
    for each_row in range(generationD_FirstRowNo, generationD_LastRowNo):
        empty_row = True
        for coln in range(1,11):
            if ws.cell(each_row,coln).value != '':
                empty_row = False
                break
        if empty_row == True:
            continue
        correctname = getcorrect_generation_ipps(ws.cell(each_row, 1).value, each_row - generationD_FirstRowNo, temp_generation_otherExchanges_list)
        if correctname != None:
            generationD_rowjson["OTHER EXCHANGES"][start_date.strftime("%d%b%Y") + " - " +str(each_row - generationD_FirstRowNo)] = {"StationName":correctname, \
                                                                                                                        "installedCapacityMW": float("%.2f" % round(ws.cell_value(each_row,5),2)) if isinstance(ws.cell(each_row,5).value,float) else 'Null',\
                                                                                                                        "atPeakinMWmax": float("%.2f" % round(ws.cell_value(each_row,7),2)) if isinstance(ws.cell(each_row,7).value,float) else 'Null',\
                                                                                                                        "atPeakinMWmin": float("%.2f" % round(ws.cell_value(each_row,8),2)) if isinstance(ws.cell(each_row,8).value,float) else 'Null',\
                                                                                                                        "generation4dayMU":float("%.3f" % round(ws.cell_value(each_row,9),3)) if isinstance(ws.cell(each_row,9).value,float) else 'Null',\
                                                                                                                        "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())}
        else:
            print(ws.cell(each_row,1).value + " does not exist in Generation IPP'S dictionary. File Number: " + os.path.basename(file))


    #generationtable1json["OTHER EXCHANGES"] = generationD_rowjson["OTHER EXCHANGES"]

    totaljson[start_date.strftime("%d%b%Y")].update({"GrandTotalInstalledCapacityMW": float("%.2f" % round(ws.cell_value(generationD_LastRowNo, 5), 2)) if isinstance(ws.cell(generationD_LastRowNo, 5).value,float) else 'Null', \
                                                    "GrandTotalPeekMaxInMW": float("%.2f" % round(ws.cell_value(generationD_LastRowNo, 7), 2)) if isinstance(ws.cell(generationD_LastRowNo, 7).value,float) else 'Null', \
                                                    "GrandTotalPeekMinInMW": float("%.2f" % round(ws.cell_value(generationD_LastRowNo, 8), 2)) if isinstance(ws.cell(generationD_LastRowNo, 8).value,float) else 'Null', \
                                                    "GrandTotalDayGenInMU": float("%.3f" % round(ws.cell_value(generationD_LastRowNo, 9), 3)) if isinstance(ws.cell(generationD_LastRowNo, 9).value,float) else 'Null', \
                                                   "date_excel": datetime.datetime.combine(current_date,datetime.datetime.min.time())})

    db.OtherExchange.insert(generationD_rowjson["OTHER EXCHANGES"].values())
    db.Total.insert(totaljson.values())

    return [generationD_LastRowNo]