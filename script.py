# !/usr/bin/env python3
# -*- coding: utf-8 -*-

import urllib.request, os, xlrd,json,difflib
from datetime import date, timedelta,datetime
from pymongo import MongoClient
from pprint import pprint
from generation import *
from config import *
from left_bottom import *
from right_top import *

#Download all Excel sheets to local filesystem
def download_files():
    path = 'http://218.248.45.137:8282/LoadCurveUpload/data/'
    delta = timedelta(days=1)
    temp_start_date=download_start_date
    while temp_start_date <= end_date:
        tag = "D" + temp_start_date.strftime("%d%b%Y") + ".xls"
        url_path = path + tag

        if os.path.exists(excelDirPath+tag)==True:
            temp_start_date += delta
            continue
        try:
            print("Trying to download excel file"+tag)
            urllib.request.urlretrieve(url_path, excelDirPath + tag)
        except:
            print("Error in downloading above file: "+ tag)
            db.download_errors.insert_one({'url_path': url_path,'file':tag,'datetime_of_error':datetime.datetime.now()})
            temp_start_date += delta
            continue
        temp_start_date += delta
        db.download_Start_date.update_one({'d_start_date': list(db.download_Start_date.find())[0]['d_start_date']}, { "$set": {'d_start_date': str(temp_start_date)}})
        #break #to be removed
    #Download completed

def processfile(file):
    try:
        wb = xlrd.open_workbook(file, formatting_info=True)
    except Exception as e:
        db.file_open_errors.insert({'file': tag, 'error':str(e), 'datetime_of_error': datetime.datetime.now()})
        return

    ws = wb.sheet_by_index(0)
    [generationD_LastRowNo] = proc_lefttable1_generation(file,ws,start_date)
    proc_leftbottom(file,ws,generationD_LastRowNo,start_date)
    proc_right_top(file,ws,start_date)
    return

#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Execution starts here~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#download_files()
db.execution_status.insert_one({'execution_start_time': datetime.datetime.now()})
while start_date <= end_date:
    tag = "D" + start_date.strftime("%d%b%Y") + ".xls"
    if os.path.exists(excelDirPath+tag) == False:
        #Save in DB-new collection of no
        start_date += oneDayinterval
        continue
    print("processing file: "+str(start_date))
    db.processing_status.insert_one({'file_name':tag,'file_processing_time': datetime.datetime.now()})
    processfile(excelDirPath + tag)
    db.Start_date.update_one({ "start_date": list(db.Start_date.find())[0]['start_date'] }, { "$set": { "start_date": str(start_date) } })
    start_date += oneDayinterval
    #os.rename(excelDirPath+tag, excelProcessed +tag)

db.execution_status.insert_one({'execution_end_time': datetime.datetime.now()})
client.close()
print("Execution over")